
## two stage fall policy
Args:
            nlu_threshold: minimum threshold for NLU confidence.
                If intent prediction confidence is lower than this,
                predict fallback action with confidence 1.0.
            core_threshold: if NLU confidence threshold is met,
                predict fallback action with confidence
                `core_threshold`. If this is the highest confidence in
                the ensemble, the fallback action will be executed.
            fallback_core_action_name: This action is executed if the Core
                threshold is not met.
            fallback_nlu_action_name: This action is executed if the user
                denies the recognised intent for the second time.
            deny_suggestion_intent_name: The name of the intent which is used
                 to detect that the user denies the suggested intents.
                 
                 
So fallback_nlu_action_name is the action called when the user denies the second time, 
and the fallback_core_action_name is the action called to propose the recognized intents. 
Wasn't clear to me with the doc, but working like a charm now

## SO IT MEANS THAT:
    - (A) first action in 2 stage policy that is called is: `fallback_core_action_name`
    - (B) fallback_nlu_action_name
    